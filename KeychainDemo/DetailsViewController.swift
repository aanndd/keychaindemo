//
//  DetailsViewController.swift
//  KeychainDemo
//
//  Created by Anand Yadav on 11/04/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        showAlert()
        let alertController = UIAlertController(title: "Add New Name", message: "", preferredStyle: .alert)

        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Second Name"
        }

        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            let secondTextField = alertController.textFields![1] as UITextField
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil )

        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter First Name"
        }

        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
        // Do any additional setup after loading the view.
    }
    
    func showAlert() {
        let alertController = UIAlertController(title: "Notes", message: "Add or Edit Notes", preferredStyle:UIAlertController.Style.alert)
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter new note"
        }
        let saveAction = UIAlertAction(title: "Add", style: UIAlertAction.Style.default) { (action) in
            let noteText = (alertController.textFields?[0])!
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel) { (action) in
                
            }
        
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
}
