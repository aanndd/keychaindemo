//
//  Notes.swift
//  KeychainDemo
//
//  Created by Anand Yadav on 11/04/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

struct Note {
    var text = ""
    var lastModified = ""
    
    init(text: String, lastModified: String) {
        self.text = text
        self.lastModified = lastModified
    }
}
