//
//  SecureManager.swift
//  KeychainDemo
//
//  Created by Anand Yadav on 11/04/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class SecureManager: NSObject {
    static func deleteKeychain(query:[String:Any]) -> Bool {
        return true
    }
    
    static func deleteLogin(username: String) -> Bool {
        return true

    }
    
    static func updateKeychain(query: [String:Any],
                               attrs: [String:Any]) -> Bool {
        return true

    }
    
    static func updatePassword(username: String, password: String) -> Bool {
        return true

    }
    
    static func findInKeychain(query : [String:Any]) -> String? {
        
        return ""
    }
    
    static func retrievePassword(username: String) -> String? {
        return ""

    }
    
    static func addToKeychain(query : [String:Any]) -> Bool {
        let result = SecItemAdd(query as CFDictionary, nil)
        let msg = SecCopyErrorMessageString(result, nil)
        print (msg!)
        return result == errSecSuccess    }
    
    static func storeServerLogin(username : String, password : String,
                                 server: String, userType: String) -> Bool {
        return true

    }
    
    static func storeLoginCredentials(username : String, password : String) -> Bool {
        
        return true

    }
    
    static func storeItem(note:Note) -> Bool {
        let tData = note.text.data(using: .utf8)!
        let query : [String:Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: note.lastModified,
                                    kSecValueData as String: tData,
                                    kSecAttrLabel as String: "note"
        ]
        return addToKeychain(query: query)
    }
    
    static func fetchItems() -> [Note]? {
        let query : [String:Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrLabel as String: "note",
                                    kSecReturnData as String: true,
                                    kSecReturnAttributes as String: true,
                                    kSecMatchLimit as String : kSecMatchLimitAll]
        
        var item:CFTypeRef?
        let result = SecItemCopyMatching(query as CFDictionary, &item)
        guard result == errSecSuccess else { return nil }
        
        guard let theItems = item as? [Dictionary<String,Any>] else { return nil }
        
        let items = theItems.compactMap { (dict) -> Note? in
            guard let data = dict[kSecValueData as String] as? Data,
                let text = String(data: data, encoding: .utf8) else { return nil }
            return Note(text: text, lastModified: dict[kSecAttrAccount as String] as! String)
        }
        
        return items
    }
    
    static func updateItem(note:Note) -> Bool {
        let tData = note.text.data(using: .utf8)!
        let query : [String:Any] = [kSecClass as String: kSecClassGenericPassword,
                                    kSecAttrAccount as String: note.lastModified,
                                    kSecAttrLabel as String: "note"]
        let attrs : [String:Any] = [kSecValueData as String: tData]
        return updateKeychain(query: query, attrs: attrs)
    }
}
