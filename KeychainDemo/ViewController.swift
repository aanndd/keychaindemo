//
//  ViewController.swift
//  KeychainDemo
//
//  Created by Anand Yadav on 11/04/20.
//  Copyright © 2020 Anand Yadav. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var notesTable: UITableView!
    var newNotes: String?
    var notes:[Note]? = []
    override func viewDidLoad() {
        super.viewDidLoad()
        notesTable.delegate = self
        notesTable.dataSource = self
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        notes = SecureManager.fetchItems()
        print(notes)
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return notes!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myCell", for: indexPath) as! MyCell
        
        cell.lblText.text = self.notes![indexPath.row].text
        cell.lblTime.text = self.notes![indexPath.row].lastModified
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = self.notesTable.cellForRow(at: indexPath) as! MyCell
        
        showAlert(existingNote: Note(text: cell.lblText.text!, lastModified: cell.lblLastModified.text!))

    }
    
    @IBAction func btnClickedAddNotes(_ sender: Any) {
        let alertController = UIAlertController(title: "Add New Notes", message: "", preferredStyle: .alert)

        alertController.addTextField { (textField : UITextField!) -> Void in
            textField.placeholder = "Enter Notes"
        }

        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            let _ = (SecureManager.storeItem(note: Note(text: firstTextField.text!, lastModified: Date().string(format: "MMM d, h:mm a"))))
            self.refreshTable()
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil )
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func btnClickedEdit(_ sender: Any) {
        
    }
    
    
    func showAlert(existingNote:Note?) {
        let alertController = UIAlertController(title: "Add New Notes", message: "", preferredStyle: .alert)

        alertController.addTextField { (textField : UITextField!) -> Void in
            if existingNote != nil {
                textField.text = existingNote?.text
            }else{
                textField.placeholder = "Enter Notes"
            }
        }

        let saveAction = UIAlertAction(title: "Save", style: .default, handler: { alert -> Void in
            let firstTextField = alertController.textFields![0] as UITextField
            if existingNote != nil {
                print(SecureManager.updateItem(note: Note(text: firstTextField.text!, lastModified: Date().string(format: "MMM d, h:mm a"))))
            }else{
                print(SecureManager.storeItem(note: Note(text: firstTextField.text!, lastModified: Date().string(format: "MMM d, h:mm a"))))
            }
        })

        let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil )
        alertController.addAction(saveAction)
        alertController.addAction(cancelAction)

        self.present(alertController, animated: true, completion: nil)
    }
    
    func refreshTable() {
        self.notes?.removeAll()
        notes = SecureManager.fetchItems()
        self.notesTable.reloadData()

    }
}


extension Date {
    func string(format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
}

//Date().string(format: "EEEE, MMM d, yyyy") // Saturday, Oct 21, 2017
//Date().string(format: "MM/dd/yyyy")        // 10/21/2017
//Date().string(format: "MM-dd-yyyy HH:mm")  // 10-21-2017 03:31
//
//Date().string(format: "MMM d, h:mm a")     // Oct 21, 3:31 AM
//Date().string(format: "MMMM yyyy")         // October 2017
//Date().string(format: "MMM d, yyyy")       // Oct 21, 2017
//
//Date().string(format: "E, d MMM yyyy HH:mm:ss Z") // Sat, 21 Oct 2017 03:31:40 +0000
//Date().string(format: "yyyy-MM-dd'T'HH:mm:ssZ")   // 2017-10-21T03:31:40+0000
//Date().string(format: "dd.MM.yy")

